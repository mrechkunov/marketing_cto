/*
 * File:   StructureLoader.hpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 14:44
 */

#ifndef _CTO_STRUCTURE_LOADER_CLASS_
#define	_CTO_STRUCTURE_LOADER_CLASS_

#include "PartnerProject.hpp"
#include <nnpcto/db/defines.hpp>
#include <map>

namespace nnpcto {
namespace cto {

class StructureLoader
{
public:
	StructureLoader(nnpcto::db::Connection connection);

	virtual ~StructureLoader();

	Partner* loadStructure(std::map<long, Partner*>& partners);
	void destroyStructure(std::map<long, Partner*>& partners);
	void loadPurchases(std::map<long, Partner*>& partners, unsigned short periodId, unsigned char project);

private:
	nnpcto::db::Connection _connection;

private:
	StructureLoader(const StructureLoader&);
	StructureLoader& operator = (const StructureLoader&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_STRUCTURE_LOADER_CLASS_ */

