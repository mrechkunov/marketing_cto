/*
 * File:   Rank.hpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 16:39
 */

#ifndef _CTO_RANK_CLASS_
#define	_CTO_RANK_CLASS_

#include "PartnerProject.hpp"

namespace nnpcto {
namespace cto {

class Rank
{
public:
	Rank(char id, unsigned char purchaseBonusPercent, unsigned char goPercentsCount);
	virtual ~Rank();

	char getId() const;

	unsigned char getPurchasePercent() const;
	unsigned char getGroupPurchasePercent(unsigned char depth) const;
	void setGroupPurchasePercent(unsigned char depth, unsigned char percent);
	unsigned char getLeadingPercent() const;
	void setLeadingPercent(unsigned char percent);

	void addPartner(PartnerProject* partner);
	unsigned long getPartnersCount() const;

private:
	char _id;
	unsigned char _loPercent;
	unsigned char *_goPercents;
	unsigned char _percentsCount;
	unsigned char _leadingPercent;
	unsigned long _partners;

private:
	Rank(const Rank&);
	Rank& operator = (const Rank&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_RANK_CLASS_ */

