/*
 * File:   Partner.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 12:41
 */

#include "Partner.hpp"
#include "PartnerProject.hpp"
#include "Application.hpp"
#include "Rank.hpp"

using namespace nnpcto::cto;

unsigned char Partner::_projectsCount = 0;

Partner::Partner(long id, long userId) : _id(id), _userId(userId)
{
	unsigned char projectsCount = _getProjectsCount();
	char i;

	_projectPartners = (PartnerProject**) malloc(sizeof(PartnerProject*) * projectsCount);
	if (_projectPartners == NULL) {
		throw "Memory allocation exception";
	}
	for (i = 0; i < projectsCount; i++) {
		_projectPartners[i] = new PartnerProject(this);
		if (_projectPartners[i] == NULL) {
			throw "Memory allocation exception";
		}
	}
	_firstChild = _lastChild = _parent = _nextSibling = NULL;
	_childrenCount = 0;
}

Partner::Partner(const Partner&)
{
	throw "Not supported";
}

Partner& Partner::operator = (const Partner&)
{
	throw "Not supported";
}

Partner::~Partner()
{
	unsigned char projectsCount = _getProjectsCount();
	char i;

	if (_projectPartners != NULL) {
		for (i = 0; i < projectsCount; i++) {
			if (_projectPartners[i] != NULL) {
				delete _projectPartners[i];
			}
		}
		std::free(_projectPartners);
	}
}

unsigned char Partner::_getProjectsCount()
{
	if (_projectsCount == 0) {
		_projectsCount = Application::getInstance()->getProjectsCount();
	}
	return _projectsCount;
}

void Partner::addChild(Partner *child)
{
	unsigned char projectsCount = _getProjectsCount();
	char i;

	if (_firstChild == NULL) {
		_firstChild = child;
		_lastChild = child;
	}
	else {
		_lastChild->_nextSibling = child;
		_lastChild = child;
	}
	child->_parent = this;
	_childrenCount++;

	for (i = 0; i < projectsCount; i++) {
		_projectPartners[i]->addChild(child->_projectPartners[i]);
	}
}

PartnerProject* Partner::getPartner(unsigned char projectId)
{
	if (projectId > 0 && projectId <= _getProjectsCount()) {
		return _projectPartners[projectId - 1];
	}
	throw "Invalid project id";
}

const PartnerProject* Partner::getPartner(unsigned char projectId) const
{
	if (projectId > 0 && projectId <= _getProjectsCount()) {
		return _projectPartners[projectId - 1];
	}
	throw "Invalid project id";
}

unsigned long Partner::getGroupPurchase() const
{
	const PartnerProject *p, *ctoPartner;
	unsigned long go;

	ctoPartner = getPartner(Application::CTO);
	go = ctoPartner->getGroupPurchase();

	p = getPartner(Application::VIRTA);
	if (p->getIsActive()) {
		// if ((float) p->getGroupPurchase() >= 0.05 * (float)ctoPartner->getGroupPurchase()) {
			go += p->getGroupPurchase();
		// }
	}
	if (ctoPartner->getPurchase() >= 500) {
		p = getPartner(Application::NANO);
		if (p->getRank()->getId() >= 0) {
			go += p->getGroupPurchase();
		}

		p = getPartner(Application::PRO100);
		if (p->getRank()->getId() >= 0) {
			go += p->getGroupPurchase();
		}
	}
	return go;
}

unsigned long Partner::getGroupPurchaseFiveLevels() const
{
	const PartnerProject *p, *ctoPartner;
	unsigned long go = 0;

	ctoPartner = getPartner(Application::CTO);
	if (ctoPartner->getPurchase() > 0) {
		go += ctoPartner->getGroupPurchaseFiveLevels();
	}

	p = getPartner(Application::VIRTA);
	if (p->getIsActive()) {
		// if ((float) p->getGroupPurchase() >= 0.05 * (float) ctoPartner->getGroupPurchase()) {
			go += p->getGroupPurchaseFiveLevels();
		// }
	}

	p = getPartner(Application::NANO);
	if (p->getRank()->getId() >= 0) {
		go += p->getGroupPurchaseFiveLevels();
	}

	if (ctoPartner->getPurchase() >= 500) {
		p = getPartner(Application::PRO100);
		if (p->getRank()->getId() > 0) {
			go += p->getGroupPurchaseFiveLevels();
		}
	}

	return go;
}

unsigned long Partner::getBonus() const
{
	double bonus = .0;
	unsigned char i;
	unsigned char count = _getProjectsCount();

	for (i = 0; i < count; ++i) {
		bonus += _projectPartners[i]->getBonus();
	}
	return ((unsigned long) ((.99 * bonus) / 10.0)) * 10L;
}
