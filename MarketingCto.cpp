/*
 * File:   MarketingCto.cpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 11:10
 */

#include "MarketingCto.hpp"
#include "Application.hpp"

#include <iostream>

using namespace nnpcto::cto;

MarketingCto::MarketingCto()
{
	_ranks = (Rank**) std::malloc(sizeof(Rank*) * 9);

	// компрессированные партнёры
	_ranks[0] = new Rank(-1, 0, 0);

	// партнёры, попавшие в расчёт, но не закупившиеся на 300 баллов
	_ranks[1] = new Rank(0, 0, 0);

	// партнёр
	_ranks[2] = new Rank(1, 15, 1);
	_ranks[2]->setGroupPurchasePercent(0, 9);

	// менеджер
	_ranks[3] = new Rank(2, 15, 5);
	_ranks[3]->setGroupPurchasePercent(0, 9);
	_ranks[3]->setGroupPurchasePercent(1, 8);
	_ranks[3]->setGroupPurchasePercent(2, 7);
	_ranks[3]->setGroupPurchasePercent(3, 6);
	_ranks[3]->setGroupPurchasePercent(4, 5);

	// старший менеджер
	_ranks[4] = new Rank(3, 15, 5);
	_ranks[4]->setGroupPurchasePercent(0, 9);
	_ranks[4]->setGroupPurchasePercent(1, 8);
	_ranks[4]->setGroupPurchasePercent(2, 7);
	_ranks[4]->setGroupPurchasePercent(3, 6);
	_ranks[4]->setGroupPurchasePercent(4, 5);
	_ranks[4]->setLeadingPercent(3);

	// лидер
	_ranks[5] = new Rank(4, 15, 5);
	_ranks[5]->setGroupPurchasePercent(0, 9);
	_ranks[5]->setGroupPurchasePercent(1, 8);
	_ranks[5]->setGroupPurchasePercent(2, 7);
	_ranks[5]->setGroupPurchasePercent(3, 6);
	_ranks[5]->setGroupPurchasePercent(4, 5);
	_ranks[5]->setLeadingPercent(5);

	// координатор
	_ranks[6] = new Rank(5, 15, 5);
	_ranks[6]->setGroupPurchasePercent(0, 9);
	_ranks[6]->setGroupPurchasePercent(1, 8);
	_ranks[6]->setGroupPurchasePercent(2, 7);
	_ranks[6]->setGroupPurchasePercent(3, 6);
	_ranks[6]->setGroupPurchasePercent(4, 5);
	_ranks[6]->setLeadingPercent(7);

	// старший координатор
	_ranks[7] = new Rank(6, 15, 5);
	_ranks[7]->setGroupPurchasePercent(0, 9);
	_ranks[7]->setGroupPurchasePercent(1, 8);
	_ranks[7]->setGroupPurchasePercent(2, 7);
	_ranks[7]->setGroupPurchasePercent(3, 6);
	_ranks[7]->setGroupPurchasePercent(4, 5);
	_ranks[7]->setLeadingPercent(9);

	// директор
	_ranks[8] = new Rank(7, 15, 5);
	_ranks[8]->setGroupPurchasePercent(0, 9);
	_ranks[8]->setGroupPurchasePercent(1, 8);
	_ranks[8]->setGroupPurchasePercent(2, 7);
	_ranks[8]->setGroupPurchasePercent(3, 6);
	_ranks[8]->setGroupPurchasePercent(4, 5);
	_ranks[8]->setLeadingPercent(10);

	_funds = (Fund**) std::malloc(sizeof(Fund*) * 5);
	_funds[0] = new Fund(0, 0);
	_funds[1] = new Fund(1, .5f);
	_funds[2] = new Fund(2, .5f);
	_funds[3] = new Fund(3, .5f);
	_funds[4] = new Fund(4, .5f);
}

MarketingCto::MarketingCto(const MarketingCto&)
{
	throw "Not supported";
}

MarketingCto& MarketingCto::operator = (const MarketingCto&)
{
	throw "Not supported";
}

MarketingCto::~MarketingCto()
{
	char i;

	if (_ranks != NULL) {
		for (i = 0; i < 9; i++) {
			delete _ranks[i];
		}
		std::free(_ranks);
		for (i = 0; i < 5; i++) {
			delete _funds[i];
		}
	}
}

void MarketingCto::calculateGroupPurchase(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;
	PartnerProject* parent;
	unsigned char depth;

	std::cout << "CTO group purchases calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::CTO);
		if (partner->getPurchase() > 0) {
			depth = 1;
			parent = partner->getParent();
			while (parent != NULL) {
				parent->addGroupPurchase(partner->getPurchase(), depth);
				depth++;
				parent = parent->getParent();
			}
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingCto::compress(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "CTO compressing";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::CTO);
		if (partner->getPurchase() > 0) {
			partner->setIsActive();
		}
		else {
			if (partner->getParent() != NULL && partner->getParent()->getIsQualified()) {
				partner->compress();
			}
		}
		if (partner->getPurchase() >= 3000) {
			partner->setIsQualified();
		}
		if (partner->getPurchase() >= 500) {
			partner->setIsRegistered();
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingCto::calculateRanks(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;
	unsigned long go;

	std::cout << "CTO ranks calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::CTO);
		if (!partner->getIsActive()) {
			_ranks[0]->addPartner(partner);
			continue;
		}
		if (partner->getPurchase() < 500) {
			_ranks[1]->addPartner(partner);
			continue;
		}
		if (!partner->getIsQualified()) {
			_ranks[2]->addPartner(partner);
			continue;
		}
		go = i->second->getGroupPurchase();
		if (go < 30000) {
			_ranks[3]->addPartner(partner);
			continue;
		}
		if (go < 100000) {
			_ranks[4]->addPartner(partner);
			continue;
		}
		if (go < 300000) {
			_ranks[5]->addPartner(partner);
			continue;
		}
		if (go < 700000) {
			_ranks[6]->addPartner(partner);
			continue;
		}
		if (go < 1000000) {
			_ranks[7]->addPartner(partner);
			continue;
		}
		_ranks[8]->addPartner(partner);
	}
	std::cout << " complete" << std::endl;
	for (char j = 0; j < 9; j++) {
		std::cout << "    Rank#" << (int) (j - 1) << ": " << _ranks[j]->getPartnersCount() << std::endl;
	}
	std::cout.flush();
}

void MarketingCto::calculateFunds(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;
	unsigned long go;

	std::cout << "CTO clubs calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::CTO);
		if (!partner->getIsActive() || partner->getPurchase() < 500) {
			_funds[0]->addPartner(partner);
			continue;
		}
		go = i->second->getGroupPurchaseFiveLevels();
		if (!partner->getIsQualified() || go < 1000000) {
			_funds[0]->addPartner(partner);
			continue;
		}
		if (go < 3000000) {
			_funds[1]->addPartner(partner);
			continue;
		}
		if (go < 7000000) {
			_funds[2]->addPartner(partner);
			continue;
		}
		if (go < 15000000) {
			_funds[3]->addPartner(partner);
			continue;
		}
		_funds[4]->addPartner(partner);
	}
	partner = partners.at(0)->getPartner(Application::CTO);
	_funds[1]->setStructurePurchase(partner->getGroupPurchase());
	_funds[2]->setStructurePurchase(partner->getGroupPurchase());
	_funds[3]->setStructurePurchase(partner->getGroupPurchase());
	_funds[4]->setStructurePurchase(partner->getGroupPurchase());
	std::cout << " complete" << std::endl;
	for (char j = 0; j < 5; j++) {
		std::cout << "    Club#" << (int)j << ": " << _funds[j]->getPartnersCount() << std::endl;
	}
	std::cout.flush();
}

void MarketingCto::calculateUnqualifiedBonuses(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;
	PartnerProject* parent;
	Rank *rank;

	std::cout << "CTO unqualified bonuses calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::CTO);
		rank = partner->getRank();
		if (partner->getPurchase() > 0 && rank->getId() <= 1) {
			parent = partner->getParent();
			if (parent != NULL) {
				rank = parent->getRank();
				if (rank->getId() == 1) {
					parent->addBonus(partner, Application::BONUS_GO, 9, 0);
				}
			}
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingCto::calculateBonuses(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	unsigned char depth;
	unsigned char maxLeadingPercent;
	PartnerProject* partner;
	PartnerProject* parent;
	Rank *rank;

	std::cout << "CTO bonuses calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		maxLeadingPercent = 0;
		partner = i->second->getPartner(Application::CTO);
		rank = partner->getRank();
		if (rank->getId() > 0) {
			partner->setPurchaseBonus(rank->getPurchasePercent());
			if (!partner->getIsPreviousRegistered()) {
				partner->setRegisterAmount(100);
			}
		}
		if (partner->getIsActive()) {
			parent = partner->getParent();
			depth = 0;
			while (parent != NULL) {
				rank = parent->getRank();
				if (rank->getId() > 0) {
					if (rank->getId() > 1) {
						if (rank->getGroupPurchasePercent(depth) > 0) {
							parent->addBonus(partner, Application::BONUS_GO, rank->getGroupPurchasePercent(depth), depth);
						}
						if (rank->getLeadingPercent() > maxLeadingPercent) {
							parent->addBonus(partner, Application::BONUS_LEADING, rank->getLeadingPercent() - maxLeadingPercent, depth);
							maxLeadingPercent = rank->getLeadingPercent();
						}
					}
					depth++;
				}
				if (rank->getId() == 0) {
					depth++;
				}
				parent = parent->getParent();
			}
			if (partner->getFund()->getId() > 0) {
				partner->setStructureBonus(partner->getFund()->getBonus());
			}
		}
	}
	std::cout << " complete" << std::endl;
}

