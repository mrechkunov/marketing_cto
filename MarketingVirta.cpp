/*
 * File:   MarketingVirta.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 14:29
 */

#include "MarketingVirta.hpp"
#include "Application.hpp"
#include <iostream>

using namespace nnpcto::cto;

MarketingVirta::MarketingVirta()
{
	_ranks = (Rank**) std::malloc(sizeof(Rank*) * 9);

	// компрессированные партнёры
	_ranks[0] = new Rank(-1, 0, 0);

	// партнёры, попавшие в расчёт, но не закупившиеся на 300 баллов
	_ranks[1] = new Rank(0, 0, 0);

	// партнёр
	_ranks[2] = new Rank(1, 10, 2);
	_ranks[2]->setGroupPurchasePercent(0, 8);
	_ranks[2]->setGroupPurchasePercent(1, 9);

	// менеджер
	_ranks[3] = new Rank(2, 10, 3);
	_ranks[3]->setGroupPurchasePercent(0, 7);
	_ranks[3]->setGroupPurchasePercent(1, 8);
	_ranks[3]->setGroupPurchasePercent(2, 9);

	// старший менеджер
	_ranks[4] = new Rank(3, 10, 5);
	_ranks[4]->setGroupPurchasePercent(0, 5);
	_ranks[4]->setGroupPurchasePercent(1, 6);
	_ranks[4]->setGroupPurchasePercent(2, 7);
	_ranks[4]->setGroupPurchasePercent(3, 8);
	_ranks[4]->setGroupPurchasePercent(4, 9);
	_ranks[4]->setLeadingPercent(6);

	// лидер
	_ranks[5] = new Rank(4, 10, 6);
	_ranks[5]->setGroupPurchasePercent(0, 4);
	_ranks[5]->setGroupPurchasePercent(1, 5);
	_ranks[5]->setGroupPurchasePercent(2, 6);
	_ranks[5]->setGroupPurchasePercent(3, 7);
	_ranks[5]->setGroupPurchasePercent(4, 8);
	_ranks[5]->setGroupPurchasePercent(5, 9);
	_ranks[5]->setLeadingPercent(7);

	// координатор
	_ranks[6] = new Rank(5, 10, 7);
	_ranks[6]->setGroupPurchasePercent(0, 3);
	_ranks[6]->setGroupPurchasePercent(1, 4);
	_ranks[6]->setGroupPurchasePercent(2, 5);
	_ranks[6]->setGroupPurchasePercent(3, 6);
	_ranks[6]->setGroupPurchasePercent(4, 7);
	_ranks[6]->setGroupPurchasePercent(5, 8);
	_ranks[6]->setGroupPurchasePercent(6, 9);
	_ranks[6]->setLeadingPercent(8);

	// старший координатор
	_ranks[7] = new Rank(6, 10, 8);
	_ranks[7]->setGroupPurchasePercent(0, 2);
	_ranks[7]->setGroupPurchasePercent(1, 3);
	_ranks[7]->setGroupPurchasePercent(2, 4);
	_ranks[7]->setGroupPurchasePercent(3, 5);
	_ranks[7]->setGroupPurchasePercent(4, 6);
	_ranks[7]->setGroupPurchasePercent(5, 7);
	_ranks[7]->setGroupPurchasePercent(6, 8);
	_ranks[7]->setGroupPurchasePercent(7, 9);
	_ranks[7]->setLeadingPercent(9);

	// директор
	_ranks[8] = new Rank(7, 10, 9);
	_ranks[8]->setGroupPurchasePercent(0, 1);
	_ranks[8]->setGroupPurchasePercent(1, 2);
	_ranks[8]->setGroupPurchasePercent(2, 3);
	_ranks[8]->setGroupPurchasePercent(3, 4);
	_ranks[8]->setGroupPurchasePercent(4, 5);
	_ranks[8]->setGroupPurchasePercent(5, 6);
	_ranks[8]->setGroupPurchasePercent(6, 7);
	_ranks[8]->setGroupPurchasePercent(7, 8);
	_ranks[8]->setGroupPurchasePercent(8, 9);
	_ranks[8]->setLeadingPercent(10);
}

MarketingVirta::MarketingVirta(const MarketingVirta&)
{
	throw "Not supported";
}

MarketingVirta& MarketingVirta::operator = (const MarketingVirta&)
{
	throw "Not supported";
}

MarketingVirta::~MarketingVirta()
{
	if (_ranks != NULL) {
		for (char i = 0; i < 9; i++) {
			delete _ranks[i];
		}
		std::free(_ranks);
	}
}

void MarketingVirta::calculateGroupPurchase(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;
	PartnerProject* parent;
	unsigned char depth;

	std::cout << "Virta group purchases calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::VIRTA);
		if (partner->getPurchase() > 0) {
			depth = 1;
			parent = partner->getParent();
			while (parent != NULL) {
				parent->addGroupPurchase(partner->getPurchase(), depth);
				depth++;
				parent = parent->getParent();
			}
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingVirta::compress(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "Virta compressing";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::VIRTA);
		if (partner->getPurchase() > 0) {
			partner->setIsActive();
		}
		else {
			partner->compress();
		}
		if (partner->getPurchase() >= 1000) {
			partner->setIsQualified();
		}
		if (partner->getPurchase() >= 300) {
			partner->setIsRegistered();
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingVirta::calculateRanks(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "Virta ranks calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::VIRTA);
		if (!partner->getIsActive()) {
			_ranks[0]->addPartner(partner);
			continue;
		}
		if (partner->getPurchase() < 300) {
			_ranks[1]->addPartner(partner);
			continue;
		}
		if (!partner->getIsQualified()) {
			_ranks[2]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 30000) {
			_ranks[3]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 100000) {
			_ranks[4]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 300000) {
			_ranks[5]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 700000) {
			_ranks[6]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 1000000) {
			_ranks[7]->addPartner(partner);
			continue;
		}
		_ranks[8]->addPartner(partner);
	}
	std::cout << " complete" << std::endl;
	for (char j = 0; j < 9; j++) {
		std::cout << "    Rank#" << (int) (j - 1) << ": " << _ranks[j]->getPartnersCount() << std::endl;
	}
	std::cout.flush();
}

void MarketingVirta::calculateBonuses(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	unsigned char depth;
	unsigned char maxLeadingPercent;
	PartnerProject* partner;
	PartnerProject* parent;
	Rank *rank;

	std::cout << "Virta bonuses calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		maxLeadingPercent = 0;
		partner = i->second->getPartner(Application::VIRTA);
		rank = partner->getRank();
		if (rank->getId() > 0) {
			partner->setPurchaseBonus(rank->getPurchasePercent());
			if (!partner->getIsPreviousRegistered()) {
				partner->setRegisterAmount(30);
			}
		}
		if (partner->getIsActive()) {
			parent = partner->getParent();
			depth = 0;
			while (parent != NULL) {
				rank = parent->getRank();
				if (rank->getId() > 0) {
					if (rank->getId() == 1) {
						if (depth == 0) {
							parent->addBonus(partner, Application::BONUS_GO, 8, depth);
						}
						else if (depth == 1) {
							parent->addBonus(partner, Application::BONUS_GO, 9, depth);
						}
					}
					else {
						if (rank->getGroupPurchasePercent(depth) > 0) {
							parent->addBonus(partner, Application::BONUS_GO, rank->getGroupPurchasePercent(depth), depth);
						}
						if (rank->getLeadingPercent() > maxLeadingPercent) {
							parent->addBonus(partner, Application::BONUS_LEADING, rank->getLeadingPercent() - maxLeadingPercent, depth);
							maxLeadingPercent = rank->getLeadingPercent();
						}
					}
					depth++;
				}
				if (rank->getId() == 0) {
					depth++;
				}
				parent = parent->getParent();
			}
		}
	}
	std::cout << " complete" << std::endl;
}