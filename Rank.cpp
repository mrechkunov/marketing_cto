/*
 * File:   Rank.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 16:39
 */

#include "Rank.hpp"
#include <cstdlib>

using namespace nnpcto::cto;

Rank::Rank(char id, unsigned char purchaseBonusPercent, unsigned char goPercentsCount)
{
	_id = id;
	_goPercents = NULL;
	_leadingPercent = 0;
	_partners = 0;
	_loPercent = purchaseBonusPercent;
	_percentsCount = goPercentsCount;
	if (goPercentsCount > 0) {
		_goPercents = (unsigned char*) std::malloc(sizeof(unsigned char) * goPercentsCount);
	}
}

Rank::Rank(const Rank&)
{
	throw "Not supported";
}

Rank::~Rank()
{
	if (_goPercents != NULL) {
		std::free(_goPercents);
	}
}

char Rank::getId() const
{
	return _id;
}

unsigned char Rank::getPurchasePercent() const
{
	return _loPercent;
}

unsigned char Rank::getGroupPurchasePercent(unsigned char depth) const
{
	if (depth < _percentsCount) {
		return _goPercents[depth];
	}
	return 0;
}

void Rank::setGroupPurchasePercent(unsigned char depth, unsigned char percent)
{
	if (depth < _percentsCount) {
		_goPercents[depth] = percent;
	}
	else {
		throw "Invalid depth for go percents";
	}
}

unsigned char Rank::getLeadingPercent() const
{
	return _leadingPercent;
}

void Rank::setLeadingPercent(unsigned char percent)
{
	_leadingPercent = percent;
}

void Rank::addPartner(PartnerProject* partner)
{
	partner->setRank(this);
	_partners++;
}

unsigned long Rank::getPartnersCount() const
{
	return _partners;
}