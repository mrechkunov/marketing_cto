/*
 * File:   PartnerProject.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 13:47
 */

#include <stdio.h>

#include "PartnerProject.hpp"
#include "Rank.hpp"
#include "Fund.hpp"
#include "Application.hpp"

using namespace nnpcto::cto;

PartnerProject::PartnerProject(Partner* partner) : _size(0), _lo(0), _go(0), _go5(0), _flags(0), _depth(0), _register(0)
{
	if (partner == NULL) {
		throw "Invalid partner instance in PartnerProject constructor";
	}
	_partner = partner;

	_loBonus = .0;
	_goBonus = .0;
	_leadingBonus = .0;
	_structureBonus = .0;

	_parent = _firstChild = _lastChild = _nextSibling = NULL;
	_childrenCount = 0;

	_rank = NULL;
	_fund = NULL;
}

PartnerProject::PartnerProject(const PartnerProject&)
{
	throw "Not supported";
}

PartnerProject& PartnerProject::operator = (const PartnerProject&)
{
	throw "Not supported";
}

PartnerProject::~PartnerProject()
{
	Bonus* bonus;
	std::list<Bonus*>::iterator i;

	for (i = _bonuses.begin(); i != _bonuses.end(); ++i) {
		bonus = *i;
		delete bonus;
	}
}

Partner* PartnerProject::getPartner()
{
	return _partner;
}

const Partner* PartnerProject::getPartner() const
{
	return _partner;
}

unsigned long PartnerProject::getStructureSize()
{
	PartnerProject *tmp;

	if (_size == 0) {
		if (_firstChild != NULL) {
			tmp = _firstChild;
			while (tmp != NULL) {
				_size += tmp->getStructureSize();
				tmp = tmp->_nextSibling;
			}
		}
		_size++;
	}
	return _size;
}

unsigned char PartnerProject::getDepth()
{
	if (_depth == 0) {
		if (_parent != NULL) {
			_depth = _parent->getDepth() + 1;
		}
	}
	return _depth;
}

unsigned long PartnerProject::getPurchase() const
{
	return _lo;
}

void PartnerProject::setPurchase(unsigned long value)
{
	_lo = value;
}

unsigned long PartnerProject::getGroupPurchase() const
{
	return _lo + _go;
}

unsigned long PartnerProject::getGroupPurchaseFiveLevels() const
{
	return _lo + _go5;
}

void PartnerProject::addGroupPurchase(unsigned long purchase, unsigned char depth)
{
	_go += purchase;
	if (depth <= 5) {
		_go5 += purchase;
	}
}

unsigned long PartnerProject::getSeparation()
{
	unsigned long maxGo = 0;
	PartnerProject *tmp, *maxPartner;

	if (_firstChild == NULL || _firstChild == _lastChild) {
		return 0;
	}
	tmp = _firstChild;
	while (tmp != NULL) {
		if (maxGo < tmp->getGroupPurchase()) {
			maxGo = tmp->getGroupPurchase();
			maxPartner = tmp;
		}
		tmp = tmp->_nextSibling;
	}
	if (maxGo > 0 && maxGo < this->getGroupPurchase()) {
		return getGroupPurchase() - maxGo;
	}
	return 0;
}

bool PartnerProject::getIsActive() const
{
	return (0 != (_flags & _CTO_PARTNER_PROJECT_IS_ACTIVE_));
}

void PartnerProject::setIsActive()
{
	_flags |= _CTO_PARTNER_PROJECT_IS_ACTIVE_;
}

bool PartnerProject::getIsQualified() const
{
	return (0 != (_flags & _CTO_PARTNER_PROJECT_IS_QUALIFIED_));
}

bool PartnerProject::getIsPreviousQualified() const
{
	return (0 != (_flags & _CTO_PARTNER_PROJECT_IS_PREV_QUALIFIED_));
}

void PartnerProject::setIsQualified(bool previousQualified)
{
	_flags |= _CTO_PARTNER_PROJECT_IS_QUALIFIED_;
	if (previousQualified) {
		_flags |= _CTO_PARTNER_PROJECT_IS_PREV_QUALIFIED_;
	}
}

bool PartnerProject::getIsRegistered() const
{
	return (0 != (_flags & _CTO_PARTNER_PROJECT_IS_REGISTERED_));
}

bool PartnerProject::getIsPreviousRegistered() const
{
	return (0 != (_flags & _CTO_PARTNER_PROJECT_IS_PREV_REGISTERED_));
}

void PartnerProject::setIsRegistered(bool previousRegistration)
{
	_flags |= _CTO_PARTNER_PROJECT_IS_REGISTERED_;
	if (previousRegistration) {
		_flags |= _CTO_PARTNER_PROJECT_IS_PREV_REGISTERED_;
	}
}

Rank* PartnerProject::getRank() const
{
	return _rank;
}

void PartnerProject::setRank(Rank* rank)
{
	_rank = rank;
}

Fund* PartnerProject::getFund() const
{
	return _fund;
}

void PartnerProject::setFund(Fund* fund)
{
	_fund = fund;
}

PartnerProject* PartnerProject::getParent()
{
	return _parent;
}

const PartnerProject* PartnerProject::getParent() const
{
	return _parent;
}

void PartnerProject::addChild(PartnerProject *child)
{
	if (_firstChild == NULL) {
		_firstChild = _lastChild = child;
	}
	else {
		_lastChild->_nextSibling = child;
		_lastChild = child;
	}
	child->_parent = this;
	_childrenCount++;
}

void PartnerProject::compress()
{
	PartnerProject *previous, *tmp;

	if (_parent == NULL || _parent->_parent == NULL || _parent->_parent->_parent == NULL) {
		// не даём компресиировать -1, 0 и 1
		return;
	}
	previous = _parent->_firstChild;
	if (_parent->_firstChild != this) {
		while (previous->_nextSibling != this) {
			previous = previous->_nextSibling;
		}
		previous->_nextSibling = this->_nextSibling;
		if (_parent->_lastChild == this) {
			_parent->_lastChild = previous;
		}
	}
	else {
		_parent->_firstChild = _nextSibling;
		if (_parent->_lastChild == this) {
			_parent->_lastChild = NULL;
		}
	}
	_parent->_childrenCount--;
	if (_firstChild != NULL) {
		tmp = _firstChild;
		while (tmp != NULL) {
			tmp->_parent = _parent;
			tmp = tmp->_nextSibling;
		}
		if (_parent->_lastChild != NULL) {
			_parent->_lastChild->_nextSibling = _firstChild;
			_parent->_lastChild = _lastChild;
			_parent->_childrenCount += _childrenCount;
		}
		else {
			_parent->_firstChild = _firstChild;
			_parent->_lastChild = _lastChild;
			_parent->_childrenCount = _childrenCount;
		}
	}
	_parent = NULL;
	_firstChild = _lastChild = NULL;
	_nextSibling = NULL;
	_childrenCount = 0;
}

void PartnerProject::setPurchaseBonus(unsigned char percent)
{
	_loBonus = ((float) _lo) * ((float) percent) / 100.0f;
	if (_loBonus > .0f) {
		_bonuses.push_back(new Bonus(this, Application::BONUS_LO, percent, _loBonus, 0));
	}
}

void PartnerProject::addBonus(PartnerProject* partner, char bonusType, unsigned char percent, unsigned char depth)
{
	float amount;

	amount = ((float) partner->getPurchase()) * ((float) percent) / 100.0f;
	switch (bonusType) {
		case Application::BONUS_GO:
			_goBonus += amount;
			_bonuses.push_back(new Bonus(partner, bonusType, percent, amount, depth));
			break;
		case Application::BONUS_LEADING:
			_leadingBonus += amount;
			_bonuses.push_back(new Bonus(partner, bonusType, percent, amount, depth));
			break;
		default:
			break;
	}
}

void PartnerProject::setStructureBonus(float bonus)
{
	_structureBonus = bonus;
	if (_structureBonus > .0f) {
		_bonuses.push_back(new Bonus(NULL, Application::BONUS_CLUB, 0, _structureBonus, 0));
	}
}

void PartnerProject::addStructureBonus(float bonus)
{
	_structureBonus += bonus;
}

void PartnerProject::bindStructureBonus()
{
	if (_structureBonus > .0f) {
		_bonuses.push_back(new Bonus(NULL, Application::BONUS_CLUB, 0, _structureBonus, 0));
	}
}

double PartnerProject::getGroupPurchaseBonus() const
{
	return _goBonus;
}

double PartnerProject::getLeadingBonus() const
{
	return _leadingBonus;
}

double PartnerProject::getStructureBonus() const
{
	return _structureBonus;
}

double PartnerProject::getPurchaseBonus() const
{
	return _loBonus;
}

double PartnerProject::getBonus() const
{
	return _loBonus + _goBonus + _leadingBonus + _structureBonus - (double) _register;
}

const std::list<Bonus*>& PartnerProject::getBonuses() const
{
	return _bonuses;
}

void PartnerProject::setRegisterAmount(unsigned char amount)
{
	_register = amount;
}

unsigned char PartnerProject::getRegisterAmount() const
{
	return _register;
}