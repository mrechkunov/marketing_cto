/*
 * File:   MarketingProsto.cpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 10:28
 */

#include "MarketingProsto.hpp"
#include "Application.hpp"

#include <iostream>

using namespace nnpcto::cto;

MarketingProsto::MarketingProsto()
{
	_ranks = (Rank**) std::malloc(sizeof(Rank*) * 9);

	// компрессированные партнёры
	_ranks[0] = new Rank(-1, 0, 0);

	// партнёры, попавшие в расчёт, но не закупившиеся на 300 баллов
	_ranks[1] = new Rank(0, 0, 0);

	// партнёр
	_ranks[2] = new Rank(1, 0, 1);
	_ranks[2]->setGroupPurchasePercent(0, 9);

	// менеджер
	_ranks[3] = new Rank(2, 0, 5);
	_ranks[3]->setGroupPurchasePercent(0, 9);
	_ranks[3]->setGroupPurchasePercent(1, 8);
	_ranks[3]->setGroupPurchasePercent(2, 7);
	_ranks[3]->setGroupPurchasePercent(3, 6);
	_ranks[3]->setGroupPurchasePercent(4, 5);

	// старший менеджер
	_ranks[4] = new Rank(3, 0, 5);
	_ranks[4]->setGroupPurchasePercent(0, 9);
	_ranks[4]->setGroupPurchasePercent(1, 8);
	_ranks[4]->setGroupPurchasePercent(2, 7);
	_ranks[4]->setGroupPurchasePercent(3, 6);
	_ranks[4]->setGroupPurchasePercent(4, 5);
	_ranks[4]->setLeadingPercent(3);

	// лидер
	_ranks[5] = new Rank(4, 0, 5);
	_ranks[5]->setGroupPurchasePercent(0, 9);
	_ranks[5]->setGroupPurchasePercent(1, 8);
	_ranks[5]->setGroupPurchasePercent(2, 7);
	_ranks[5]->setGroupPurchasePercent(3, 6);
	_ranks[5]->setGroupPurchasePercent(4, 5);
	_ranks[5]->setLeadingPercent(5);

	// координатор
	_ranks[6] = new Rank(5, 0, 5);
	_ranks[6]->setGroupPurchasePercent(0, 9);
	_ranks[6]->setGroupPurchasePercent(1, 8);
	_ranks[6]->setGroupPurchasePercent(2, 7);
	_ranks[6]->setGroupPurchasePercent(3, 6);
	_ranks[6]->setGroupPurchasePercent(4, 5);
	_ranks[6]->setLeadingPercent(7);

	// старший координатор
	_ranks[7] = new Rank(6, 0, 5);
	_ranks[7]->setGroupPurchasePercent(0, 9);
	_ranks[7]->setGroupPurchasePercent(1, 8);
	_ranks[7]->setGroupPurchasePercent(2, 7);
	_ranks[7]->setGroupPurchasePercent(3, 6);
	_ranks[7]->setGroupPurchasePercent(4, 5);
	_ranks[7]->setLeadingPercent(9);

	// директор
	_ranks[8] = new Rank(7, 0, 5);
	_ranks[8]->setGroupPurchasePercent(0, 9);
	_ranks[8]->setGroupPurchasePercent(1, 8);
	_ranks[8]->setGroupPurchasePercent(2, 7);
	_ranks[8]->setGroupPurchasePercent(3, 6);
	_ranks[8]->setGroupPurchasePercent(4, 5);
	_ranks[8]->setLeadingPercent(10);

	_funds = (Fund**) std::malloc(sizeof(Fund*) * 6);
	_funds[0] = new Fund(0, 0);
	_funds[1] = new Fund(1, 5);
	_funds[2] = new Fund(2, 4);
	_funds[3] = new Fund(3, 3);
	_funds[4] = new Fund(4, 2);
	_funds[5] = new Fund(5, 1);
}

MarketingProsto::MarketingProsto(const MarketingProsto&)
{
	throw "Not supported";
}

MarketingProsto& MarketingProsto::operator = (const MarketingProsto&)
{
	throw "Not supported";
}

MarketingProsto::~MarketingProsto()
{
	char i;

	if (_ranks != NULL) {
		for (i = 0; i < 9; i++) {
			delete _ranks[i];
		}
		std::free(_ranks);
	}
	if (_funds != NULL) {
		for (i = 0; i < 6; i++) {
			delete _funds[i];
		}
		std::free(_funds);
	}
}

void MarketingProsto::calculateGroupPurchase(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;
	PartnerProject* parent;
	unsigned char depth;

	std::cout << "PRO100 group purchases calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::PRO100);
		if (partner->getPurchase() > 0) {
			depth = 1;
			parent = partner->getParent();
			while (parent != NULL) {
				parent->addGroupPurchase(partner->getPurchase(), depth);
				depth++;
				parent = parent->getParent();
			}
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingProsto::compress(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "PRO100 compressing";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::PRO100);
		if (partner->getPurchase() > 0) {
			partner->setIsActive();
		}
		else {
			partner->compress();
		}
		if (partner->getPurchase() >= 500) {
			partner->setIsQualified();
		}
		if (partner->getPurchase() >= 100) {
			partner->setIsRegistered();
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingProsto::calculateRanks(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "PRO100 ranks calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::PRO100);
		if (!partner->getIsActive()) {
			_ranks[0]->addPartner(partner);
			continue;
		}
		if (partner->getPurchase() < 100) {
			_ranks[1]->addPartner(partner);
			continue;
		}
		if (!partner->getIsQualified()) {
			_ranks[2]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 30000) {
			_ranks[3]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 100000) {
			_ranks[4]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 300000) {
			_ranks[5]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 700000) {
			_ranks[6]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 1000000) {
			_ranks[7]->addPartner(partner);
			continue;
		}
		_ranks[8]->addPartner(partner);
	}
	std::cout << " complete" << std::endl;
	for (char j = 0; j < 9; j++) {
		std::cout << "    Rank#" << (int) (j - 1) << ": " << _ranks[j]->getPartnersCount() << std::endl;
	}
	std::cout.flush();
}

void MarketingProsto::calculateFunds(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "PRO100 funds calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::PRO100);
		if (!partner->getIsActive()) {
			_funds[0]->addPartner(partner);
			continue;
		}
		if (partner->getPurchase() < 100 /*|| !partner->getIsQualified()*/ || partner->getSeparation() < 30000) {
			_funds[0]->addPartner(partner);
			continue;
		}
		_funds[1]->addPartner(partner);
		if (partner->getSeparation() >= 100000) {
			_funds[2]->addPartner(partner);
			if (partner->getSeparation() >= 300000) {
				_funds[3]->addPartner(partner);
				if (partner->getSeparation() >= 700000) {
					_funds[4]->addPartner(partner);
					if (partner->getSeparation() >= 1000000) {
						_funds[5]->addPartner(partner);
					}
				}
			}
		}
	}
	partner = partners.at(0)->getPartner(Application::PRO100);
	_funds[1]->setStructurePurchase(partner->getGroupPurchase());
	_funds[2]->setStructurePurchase(partner->getGroupPurchase());
	_funds[3]->setStructurePurchase(partner->getGroupPurchase());
	_funds[4]->setStructurePurchase(partner->getGroupPurchase());
	_funds[5]->setStructurePurchase(partner->getGroupPurchase());
	std::cout << " complete" << std::endl;
	for (char j = 0; j < 6; j++) {
		std::cout << "    Fund#" << (int) j << ": " << _funds[j]->getPartnersCount() << std::endl;
	}
	std::cout.flush();
}

void MarketingProsto::calculateBonuses(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	unsigned char depth;
	unsigned char maxLeadingPercent;
	PartnerProject* partner;
	PartnerProject* parent;
	Rank *rank;
	unsigned char j;

	std::cout << "PRO100 bonuses calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		maxLeadingPercent = 0;
		partner = i->second->getPartner(Application::PRO100);
		if (partner->getIsActive()) {
			parent = partner->getParent();
			depth = 0;
			while (parent != NULL) {
				rank = parent->getRank();
				if (rank->getId() > 0) {
					if (rank->getId() == 1) {
						if (partner->getRank()->getId() <= 1) {
							if (depth == 0) {
								parent->addBonus(partner, Application::BONUS_GO, 9, depth);
							}
						}
					}
					else {
						if (rank->getGroupPurchasePercent(depth) > 0) {
							parent->addBonus(partner, Application::BONUS_GO, rank->getGroupPurchasePercent(depth), depth);
						}
						if (rank->getLeadingPercent() > maxLeadingPercent) {
							parent->addBonus(partner, Application::BONUS_LEADING, rank->getLeadingPercent() - maxLeadingPercent, depth);
							maxLeadingPercent = rank->getLeadingPercent();
						}
					}
				}
				parent = parent->getParent();
				depth++;
			}
			if (partner->getFund()->getId() > 0) {
				for (j = 1; j <= partner->getFund()->getId(); j++) {
					partner->addStructureBonus(_funds[j]->getBonus());
				}
				partner->bindStructureBonus();
			}
		}
	}
	std::cout << " complete" << std::endl;
}

