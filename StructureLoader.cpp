/*
 * File:   StructureLoader.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 14:44
 */

#include "StructureLoader.hpp"
#include "Application.hpp"
#include <sstream>
#include <iostream>
#include <string>

using namespace nnpcto::cto;

StructureLoader::StructureLoader(nnpcto::db::Connection connection)
{
	_connection = connection;
}

StructureLoader::StructureLoader(const StructureLoader&)
{}

StructureLoader::~StructureLoader()
{}

Partner* StructureLoader::loadStructure(std::map<long, Partner*>& partners)
{
	std::cout << "Loading structure";
	std::cout.flush();

	nnpcto::db::Statement stmt = _connection.createStatement();
	stmt.execute(
		"SELECT " \
		"id,sponsor_id,user_id,"\
		"CASE WHEN qc_period_id IS NULL THEN FALSE ELSE TRUE END AS c_qual," \
		"CASE WHEN qv_period_id IS NULL THEN FALSE ELSE TRUE END AS v_qual," \
		"CASE WHEN qp_period_id IS NULL THEN FALSE ELSE TRUE END AS p_qual," \
		"CASE WHEN fpc_period_id IS NULL THEN FALSE ELSE TRUE END AS c_reg," \
		"CASE WHEN fpv_period_id IS NULL THEN FALSE ELSE TRUE END AS v_reg," \
		"CASE WHEN fpn_period_id IS NULL THEN FALSE ELSE TRUE END AS n_reg," \
		"CASE WHEN fpp_period_id IS NULL THEN FALSE ELSE TRUE END AS p_reg " \
		"FROM partners " \
		"WHERE id>=-1 "\
		"AND terminate_period_id IS NULL"
	);
	nnpcto::db::ResultSet res = stmt.getResultSet();
	std::map<long, long> parentIds;
	std::map<long, long>::iterator i;
	std::map<long, Partner*>::iterator prnt;
	long id, parentId;
	Partner * root = NULL;
	Partner *partner;

	while (res.next()) {
		id = res.getLong(0);
		parentId = res.getLong(1);
		partner = new Partner(id, res.getLong(2));
		partners[id] = partner;
		if (id != parentId) {
			parentIds[id] = parentId;
		}
		else {
			root = partners.at(id);
		}
		if (res.getBool(3)) {
			partner->getPartner(Application::CTO)->setIsQualified(true);
		}
		if (res.getBool(4)) {
			partner->getPartner(Application::VIRTA)->setIsQualified(true);
		}
		if (res.getBool(5)) {
			partner->getPartner(Application::PRO100)->setIsQualified(true);
		}
		if (res.getBool(6)) {
			partner->getPartner(Application::CTO)->setIsRegistered(true);
		}
		if (res.getBool(7)) {
			partner->getPartner(Application::VIRTA)->setIsRegistered(true);
		}
		if (res.getBool(8)) {
			partner->getPartner(Application::NANO)->setIsRegistered(true);
		}
		if (res.getBool(9)) {
			partner->getPartner(Application::PRO100)->setIsRegistered(true);
		}
	}
	std::cout << " complete" << std::endl;
	std::cout << "Structure building";
	std::cout.flush();
	for (i = parentIds.begin(); i != parentIds.end(); ++i) {
		prnt = partners.find(i->second);
		if (prnt != partners.end()) {
			prnt->second->addChild(partners.at(i->first));
		}
		else {
			std::cout << "Parent#" << i->second << " not found for partner#" << i->first;
			throw "Parent not found";
		}
	}
	parentIds.clear();

	std::cout << " complete" << std::endl;

	return root;
}

void StructureLoader::loadPurchases(std::map<long, Partner*>& partners, unsigned short period, unsigned char project)
{
	std::cout << "Loading purchases for project#" << (int) project;
	std::cout.flush();

	nnpcto::db::Statement stmt = _connection.createStatement();
	std::stringstream queryBuffer;
	std::map<long, Partner*>::iterator i;

	queryBuffer << "SELECT a.partner_id, SUM(a.quantity * b.price) "
			<< "FROM period_" << period << ".partner_purchases a INNER JOIN prices b ON a.articul_id=b.articul_id "
			<< "WHERE b.project_id=" << (int) project << " AND b.type='S' "
			<< "GROUP BY a.partner_id";

	stmt.execute(queryBuffer.str());
	nnpcto::db::ResultSet res = stmt.getResultSet();

	while (res.next()) {
		i = partners.find(res.getLong(0));
		if (i == partners.end()) {
			std::cout << "Partner#" << res.getLong(0) << " not found while fetching partner purchases" << std::endl;
		}
		else {
			i->second->getPartner(project)->setPurchase(res.getULong(1));
		}
	}
	std::cout << " complete" << std::endl;
}

void StructureLoader::destroyStructure(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	Partner* partner;

	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second;
		if (partner != NULL) {
			delete partner;
		}
	}
	partners.clear();
}
