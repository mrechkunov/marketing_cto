#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Application.o \
	${OBJECTDIR}/Partner.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/MarketingNano.o \
	${OBJECTDIR}/StructureWriter.o \
	${OBJECTDIR}/Rank.o \
	${OBJECTDIR}/Fund.o \
	${OBJECTDIR}/StructureLoader.o \
	${OBJECTDIR}/MarketingCto.o \
	${OBJECTDIR}/PartnerProject.o \
	${OBJECTDIR}/MarketingProsto.o \
	${OBJECTDIR}/MarketingVirta.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpq -lndbc -lndbcpgsql

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cto

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cto: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cto ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/Application.o: Application.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/Application.o Application.cpp

${OBJECTDIR}/Partner.o: Partner.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/Partner.o Partner.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/MarketingNano.o: MarketingNano.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/MarketingNano.o MarketingNano.cpp

${OBJECTDIR}/StructureWriter.o: StructureWriter.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/StructureWriter.o StructureWriter.cpp

${OBJECTDIR}/Rank.o: Rank.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/Rank.o Rank.cpp

${OBJECTDIR}/Fund.o: Fund.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/Fund.o Fund.cpp

${OBJECTDIR}/StructureLoader.o: StructureLoader.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/StructureLoader.o StructureLoader.cpp

${OBJECTDIR}/MarketingCto.o: MarketingCto.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/MarketingCto.o MarketingCto.cpp

${OBJECTDIR}/PartnerProject.o: PartnerProject.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/PartnerProject.o PartnerProject.cpp

${OBJECTDIR}/MarketingProsto.o: MarketingProsto.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/MarketingProsto.o MarketingProsto.cpp

${OBJECTDIR}/MarketingVirta.o: MarketingVirta.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -I/usr/include/postgresql -MMD -MP -MF $@.d -o ${OBJECTDIR}/MarketingVirta.o MarketingVirta.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/cto

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
