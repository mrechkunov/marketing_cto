/*
 * File:   MarketingNano.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 17:36
 */

#include "MarketingNano.hpp"
#include "Application.hpp"
#include <iostream>

using namespace nnpcto::cto;

MarketingNano::MarketingNano()
{
	_ranks = (Rank**) std::malloc(sizeof(Rank*) * 9);

	// компрессированные партнёры
	_ranks[0] = new Rank(-1, 0, 0);

	// партнёры, попавшие в расчёт, но не закупившиеся на 500 баллов
	_ranks[1] = new Rank(0, 0, 0);

	// партнёр
	_ranks[2] = new Rank(1, 20, 2);
	_ranks[2]->setGroupPurchasePercent(0, 10);
	_ranks[2]->setGroupPurchasePercent(1, 10);

	// менеджер
	_ranks[3] = new Rank(2, 20, 3);
	_ranks[3]->setGroupPurchasePercent(0, 10);
	_ranks[3]->setGroupPurchasePercent(1, 10);
	_ranks[3]->setGroupPurchasePercent(2, 10);

	// старший менеджер
	_ranks[4] = new Rank(3, 20, 4);
	_ranks[4]->setGroupPurchasePercent(0, 10);
	_ranks[4]->setGroupPurchasePercent(1, 10);
	_ranks[4]->setGroupPurchasePercent(2, 10);
	_ranks[4]->setGroupPurchasePercent(3, 5);
	_ranks[4]->setLeadingPercent(3);

	// лидер
	_ranks[5] = new Rank(4, 20, 5);
	_ranks[5]->setGroupPurchasePercent(0, 10);
	_ranks[5]->setGroupPurchasePercent(1, 10);
	_ranks[5]->setGroupPurchasePercent(2, 5);
	_ranks[5]->setGroupPurchasePercent(3, 5);
	_ranks[5]->setGroupPurchasePercent(4, 5);
	_ranks[5]->setLeadingPercent(5);

	// координатор
	_ranks[6] = new Rank(5, 20, 6);
	_ranks[6]->setGroupPurchasePercent(0, 10);
	_ranks[6]->setGroupPurchasePercent(1, 5);
	_ranks[6]->setGroupPurchasePercent(2, 5);
	_ranks[6]->setGroupPurchasePercent(3, 5);
	_ranks[6]->setGroupPurchasePercent(4, 5);
	_ranks[6]->setGroupPurchasePercent(5, 5);
	_ranks[6]->setLeadingPercent(7);

	// старший координатор
	_ranks[7] = new Rank(6, 20, 7);
	_ranks[7]->setGroupPurchasePercent(0, 5);
	_ranks[7]->setGroupPurchasePercent(1, 5);
	_ranks[7]->setGroupPurchasePercent(2, 5);
	_ranks[7]->setGroupPurchasePercent(3, 5);
	_ranks[7]->setGroupPurchasePercent(4, 5);
	_ranks[7]->setGroupPurchasePercent(5, 5);
	_ranks[7]->setGroupPurchasePercent(6, 5);
	_ranks[7]->setLeadingPercent(9);

	// директор
	_ranks[8] = new Rank(7, 20, 7);
	_ranks[8]->setGroupPurchasePercent(0, 5);
	_ranks[8]->setGroupPurchasePercent(1, 5);
	_ranks[8]->setGroupPurchasePercent(2, 5);
	_ranks[8]->setGroupPurchasePercent(3, 5);
	_ranks[8]->setGroupPurchasePercent(4, 5);
	_ranks[8]->setGroupPurchasePercent(5, 5);
	_ranks[8]->setGroupPurchasePercent(6, 5);
	_ranks[8]->setLeadingPercent(10);
}

MarketingNano::MarketingNano(const MarketingNano&)
{
	throw "Not supported";
}

MarketingNano& MarketingNano::operator = (const MarketingNano&)
{
	throw "Not supported";
}

MarketingNano::~MarketingNano()
{
	if (_ranks != NULL) {
		for (char i = 0; i < 9; i++) {
			delete _ranks[i];
		}
		std::free(_ranks);
	}
}

void MarketingNano::calculateGroupPurchase(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;
	PartnerProject* parent;
	unsigned char depth;

	std::cout << "Nano group purchases calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::NANO);
		if (partner->getPurchase() > 0) {
			depth = 1;
			parent = partner->getParent();
			while (parent != NULL) {
				parent->addGroupPurchase(partner->getPurchase(), depth);
				depth++;
				parent = parent->getParent();
			}
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingNano::compress(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "Nano compressing";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::NANO);
		if (partner->getPurchase() > 0) {
			partner->setIsActive();
		}
		else {
			partner->compress();
		}
		if (partner->getPurchase() >= 500) {
			partner->setIsRegistered();
		}
	}
	std::cout << " complete" << std::endl;
}

void MarketingNano::calculateRanks(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	PartnerProject* partner;

	std::cout << "Nano ranks calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		partner = i->second->getPartner(Application::NANO);
		if (!partner->getIsActive()) {
			_ranks[0]->addPartner(partner);
			continue;
		}
		if (partner->getPurchase() < 500) {
			_ranks[1]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 10000) {
			_ranks[2]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 30000) {
			_ranks[3]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 100000) {
			_ranks[4]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 300000) {
			_ranks[5]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 700000) {
			_ranks[6]->addPartner(partner);
			continue;
		}
		if (partner->getGroupPurchase() < 1000000) {
			_ranks[7]->addPartner(partner);
			continue;
		}
		_ranks[8]->addPartner(partner);
	}
	std::cout << " complete" << std::endl;
	for (char j = 0; j < 9; j++) {
		std::cout << "    Rank#" << (int) (j - 1) << ": " << _ranks[j]->getPartnersCount() << std::endl;
	}
	std::cout.flush();
}

void MarketingNano::calculateBonuses(std::map<long, Partner*>& partners)
{
	std::map<long, Partner*>::iterator i;
	unsigned char depth;
	unsigned char maxLeadingPercent;
	PartnerProject* partner;
	PartnerProject* parent;
	Rank *rank;

	std::cout << "Nano bonuses calculation";
	std::cout.flush();
	for (i = partners.begin(); i != partners.end(); ++i) {
		maxLeadingPercent = 0;
		partner = i->second->getPartner(Application::NANO);
		rank = partner->getRank();
		if (rank->getId() > 0) {
			if (!partner->getIsPreviousRegistered()) {
				partner->setRegisterAmount(50);
			}
			partner->setPurchaseBonus(rank->getPurchasePercent());
			parent = partner->getParent();
			depth = 0;
			while (parent != NULL) {
				rank = parent->getRank();
				if (rank->getId() > 0) {
					if (rank->getId() == 1) {
						if (partner->getRank()->getId() == 1) {
							if (depth == 0 || depth == 1) {
								parent->addBonus(partner, Application::BONUS_GO, 10, depth);
							}
							depth++;
						}
					}
					else {
						if (rank->getGroupPurchasePercent(depth) > 0) {
							parent->addBonus(partner, Application::BONUS_GO, rank->getGroupPurchasePercent(depth), depth);
						}
						if (rank->getLeadingPercent() > maxLeadingPercent) {
							parent->addBonus(partner, Application::BONUS_LEADING, rank->getLeadingPercent() - maxLeadingPercent, depth);
							maxLeadingPercent = rank->getLeadingPercent();
						}
						depth++;
					}
				}
				parent = parent->getParent();
			}
		}
	}
	std::cout << " complete" << std::endl;
}

