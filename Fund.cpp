/*
 * File:   Fund.cpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 11:57
 */

#include "Fund.hpp"

using namespace nnpcto::cto;

Fund::Fund(char id, float percent)
{
	_id = id;
	_percent = percent;
	_partners = 0;
	_bonusSize = .0f;
}

Fund::Fund(const Fund&)
{
	throw "Not supported";
}

Fund& Fund::operator = (const Fund&)
{
	throw "Not supported";
}

Fund::~Fund()
{}

char Fund::getId() const
{
	return _id;
}

float Fund::getPercent() const
{
	return _percent;
}

void Fund::addPartner(PartnerProject* partner)
{
	partner->setFund(this);
	_partners++;
}

unsigned long Fund::getPartnersCount() const
{
	return _partners;
}

void Fund::setStructurePurchase(unsigned long purchase)
{
	if (_partners > 0 && purchase > 0 && _percent > 0) {
		_bonusSize = ((float) purchase) * ((float) _percent) / 100.0f / ((float) _partners);
	}
	else {
		_bonusSize = .0f;
	}
}

float Fund::getBonus() const
{
	return _bonusSize;
}