/*
 * File:   StructureWriter.cpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 12:30
 */

#include "StructureWriter.hpp"
#include "Rank.hpp"
#include "Fund.hpp"

using namespace nnpcto::cto;

StructureWriter::StructureWriter()
{

}

StructureWriter::StructureWriter(const StructureWriter&)
{
	throw "Not supported";
}

StructureWriter& StructureWriter::operator = (const StructureWriter&)
{
	throw "Not supported";
}

StructureWriter::~StructureWriter()
{
}

void StructureWriter::save(std::map<long, Partner*> &partners, unsigned char periodId)
{
	_saveStructure(partners, periodId);
}

void StructureWriter::_saveStructure(std::map<long, Partner*> &partners, unsigned char periodId)
{
	std::stringstream fileName;
	std::fstream out;
	std::map<long, Partner*>::iterator i;

	fileName << "result_struct_" << (int) periodId << ".dat";
	out.open(fileName.str().c_str(), std::ios_base::out);
	if (!out.is_open()) {
		throw "File not opened";
	}
	std::cout << "Saving structure " << std::endl;
	out.setf(std::ios::fixed, std::ios::floatfield);
	out << std::setprecision(2);

	// _createStructStatement(out, p, periodId);
	_insertHeadStruct(out, periodId);
	for (i = partners.begin(); i != partners.end(); ++i) {
		_insertLineStruct(out, periodId, i->first, i->second);
	}
	out << "\\.\n\n";

	out.flush();
	out.close();
	std::cout << " complete" << std::endl;
}

void StructureWriter::_insertHeadStruct(std::ostream &out, unsigned char periodId)
{
	out << "COPY period_" << (int) periodId << ".partners (partner_id,user_id,period_id,clo,cgo,cgo5,crank,cclub,"
		<< "vlo,vgo,vrank,nlo,ngo,ngo5,nrank,plo,pgo,pgo5,psep,prank,pfund,b11,b12,b13,b14,b21,b22,b23,b31,b32,b33,"
		<< "b42,b43,b44,b1,b2,b3,b4,bonus,flags,go,go5) FROM stdin;\n";
}

void StructureWriter::_insertLineStruct(std::ostream &out, unsigned char periodId, long partnerId, Partner* partner)
{
	short flags = 0;

	if (partner->getPartner(1)->getIsActive()) {
		flags |= 0x0001;
	}
	if (partner->getPartner(2)->getIsActive()) {
		flags |= 0x0002;
	}
	if (partner->getPartner(3)->getIsActive()) {
		flags |= 0x0004;
	}
	if (partner->getPartner(4)->getIsActive()) {
		flags |= 0x0008;
	}
	if (partner->getPartner(1)->getIsQualified()) {
		flags |= 0x0100;
	}
	if (partner->getPartner(2)->getIsQualified()) {
		flags |= 0x0200;
	}
	/*if (partner->getPartner(3)->getIsQualified()) {
		flags |= 0x0400;
	}*/
	if (partner->getPartner(4)->getIsQualified()) {
		flags |= 0x0800;
	}
	out << partnerId << "\t"
		<< partner->getUserId() << "\t"
		<< (int) periodId << "\t"
		<< partner->getPartner(1)->getPurchase() << "\t"
		<< partner->getPartner(1)->getGroupPurchase() << "\t"
		<< partner->getPartner(1)->getGroupPurchaseFiveLevels() << "\t"
		<< (int) partner->getPartner(1)->getRank()->getId() << "\t"
		<< (int) partner->getPartner(1)->getFund()->getId() << "\t"
		<< partner->getPartner(2)->getPurchase() << "\t"
		<< partner->getPartner(2)->getGroupPurchase() << "\t"
		<< (int) partner->getPartner(2)->getRank()->getId() << "\t"
		<< partner->getPartner(3)->getPurchase() << "\t"
		<< partner->getPartner(3)->getGroupPurchase() << "\t"
		<< partner->getPartner(3)->getGroupPurchaseFiveLevels() << "\t"
		<< (int) partner->getPartner(3)->getRank()->getId() << "\t"
		<< partner->getPartner(4)->getPurchase() << "\t"
		<< partner->getPartner(4)->getGroupPurchase() << "\t"
		<< partner->getPartner(4)->getGroupPurchaseFiveLevels() << "\t"
		<< partner->getPartner(4)->getSeparation() << "\t"
		<< (int) partner->getPartner(4)->getRank()->getId() << "\t"
		<< (int) partner->getPartner(4)->getFund()->getId() << "\t"
		<< partner->getPartner(1)->getPurchaseBonus() << "\t"
		<< partner->getPartner(1)->getGroupPurchaseBonus() << "\t"
		<< partner->getPartner(1)->getLeadingBonus() << "\t"
		<< partner->getPartner(1)->getStructureBonus() << "\t"
		<< partner->getPartner(2)->getPurchaseBonus() << "\t"
		<< partner->getPartner(2)->getGroupPurchaseBonus() << "\t"
		<< partner->getPartner(2)->getLeadingBonus() << "\t"
		<< partner->getPartner(3)->getPurchaseBonus() << "\t"
		<< partner->getPartner(3)->getGroupPurchaseBonus() << "\t"
		<< partner->getPartner(3)->getLeadingBonus() << "\t"
		<< partner->getPartner(4)->getGroupPurchaseBonus() << "\t"
		<< partner->getPartner(4)->getLeadingBonus() << "\t"
		<< partner->getPartner(4)->getStructureBonus() << "\t"
		<< partner->getPartner(1)->getBonus() << "\t"
		<< partner->getPartner(2)->getBonus() << "\t"
		<< partner->getPartner(3)->getBonus() << "\t"
		<< partner->getPartner(4)->getBonus() << "\t"
		<< partner->getBonus() << "\t"
		<< flags << "\t"
		<< partner->getGroupPurchase() << "\t"
		<< partner->getGroupPurchaseFiveLevels() << "\n";
}

void StructureWriter::saveQualifications(std::map<long, Partner*> &partners, unsigned char periodId)
{
	std::stringstream fileName;
	std::fstream out;
	std::map<long, Partner*>::iterator i;

	fileName << "result_qualifications_" << (int) periodId << ".dat";
	out.open(fileName.str().c_str(), std::ios_base::out);
	if (!out.is_open()) {
		throw "File not opened";
	}
	std::cout << "Saving qualifications " << std::endl;
	out.setf(std::ios::fixed, std::ios::floatfield);
	out << std::setprecision(2);

	for (i = partners.begin(); i != partners.end(); ++i) {
		_insertLineQualification(out, periodId, i->first, i->second);
	}

	out.flush();
	out.close();
	std::cout << " complete" << std::endl;
}

void StructureWriter::_insertLineQualification(std::ostream &out, unsigned char periodId, long partnerId, Partner* parener)
{
	bool insert = false;

	insert = _insertFieldQualification(out, periodId, parener->getPartner(1), insert, 'c');
	insert = _insertFieldQualification(out, periodId, parener->getPartner(2), insert, 'v');
	insert = _insertFieldQualification(out, periodId, parener->getPartner(3), insert, 'n');
	insert = _insertFieldQualification(out, periodId, parener->getPartner(4), insert, 'p');
	if (insert) {
		out << " WHERE id=" << partnerId << ";\n";
	}
}

bool StructureWriter::_insertFieldQualification(std::ostream &out, unsigned char per, PartnerProject* par, bool ins, char prj)
{
	if (par->getIsQualified() && !par->getIsPreviousQualified()) {
		if (!ins) {
			out << "UPDATE partners SET ";
			ins = true;
		}
		else {
			out << ",";
		}
		out << 'q' << prj << "_period_id=" << (int) per;
	}
	if (par->getRank()->getId() > 0) {
		if (!ins) {
			out << "UPDATE partners SET ";
			ins = true;
		}
		else {
			out << ",";
		}
		out << "la" << prj << "_period_id=" << (int) per;
	}
	if (par->getPurchase() > 0) {
		if (!ins) {
			out << "UPDATE partners SET ";
			ins = true;
		}
		else {
			out << ",";
		}
		out << "lp" << prj << "_period_id=" << (int) per;
	}
	if (par->getIsRegistered() && !par->getIsPreviousRegistered()) {
		if (!ins) {
			out << "UPDATE partners SET ";
			ins = true;
		}
		else {
			out << ",";
		}
		out << "fp" << prj << "_period_id=" << (int) per;
	}
	return ins;
}

void StructureWriter::saveBonuses(std::map<long, Partner*> &partners, unsigned char periodId)
{
	std::stringstream fileName;
	std::fstream out;
	std::map<long, Partner*>::iterator i;

	fileName << "result_bonuses_" << (int) periodId << ".dat";
	out.open(fileName.str().c_str(), std::ios_base::out);
	if (!out.is_open()) {
		throw "File not opened";
	}
	std::cout << "Saving bonuses " << std::endl;
	out.setf(std::ios::fixed, std::ios::floatfield);
	out << std::setprecision(2);

	out << "COPY period_" << (int) periodId << ".bonuses (period_id,project_id,partner_id,source_id,type,depth,"
		<< "percent,amount) FROM stdin;\n";

	for (i = partners.begin(); i != partners.end(); ++i) {
		_insertLineBonuses(out, periodId, i->first, i->second);
	}

	out.flush();
	out.close();
	std::cout << " complete" << std::endl;
}

void StructureWriter::_insertLineBonuses(std::ostream &out, unsigned char periodId, long partnerId, Partner* partner)
{
	std::list<Bonus*> bonuses;
	std::list<Bonus*>::const_iterator i;
	Bonus* bonus;
	unsigned char projectId;
	PartnerProject *source;

	for (projectId = 1; projectId <= 4; projectId++) {
		bonuses = partner->getPartner(projectId)->getBonuses();
		for (i = bonuses.begin(); i != bonuses.end(); ++i) {
			bonus = (*i);
			source = bonus->getSourcePartner();
			out << (int) periodId << "\t"
				<< (int) projectId << "\t"
				<< partnerId << "\t";
			if (source != NULL) {
				out << (int) source->getId() << "\t";
			}
			else {
				out << "\\N\t";
			}
			out << (int) bonus->getType() << "\t"
				<< (int) bonus->getDepth() << "\t"
				<< (int) bonus->getPercent() << "\t"
				<< bonus->getAmount() << "\n";
		}
	}
}