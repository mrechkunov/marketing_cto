/*
 * File:   StructureWriter.hpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 12:30
 */

#ifndef _CTO_STRUCTURE_WRITER_CLASS_
#define	_CTO_STRUCTURE_WRITER_CLASS_

#include "Partner.hpp"
#include "Bonus.hpp"
#include <list>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>

namespace nnpcto {
namespace cto {

class StructureWriter
{
public:
	StructureWriter();
	virtual ~StructureWriter();

	void save(std::map<long, Partner*> &partners, unsigned char periodId);
	void saveQualifications(std::map<long, Partner*> &partners, unsigned char periodId);
	void saveBonuses(std::map<long, Partner*> &partners, unsigned char periodId);

private:
	void _saveStructure(std::map<long, Partner*> &partners, unsigned char periodId);
	void _insertHeadStruct(std::ostream &out, unsigned char periodId);
	void _insertLineStruct(std::ostream &out, unsigned char periodId, long partnerId, Partner* parener);
	void _insertLineQualification(std::ostream &out, unsigned char periodId, long partnerId, Partner* parener);
	bool _insertFieldQualification(std::ostream &out, unsigned char per, PartnerProject* par, bool ins, char prj);
	void _insertLineBonuses(std::ostream &out, unsigned char periodId, long partnerId, Partner* partner);

private:
	StructureWriter(const StructureWriter&);
	StructureWriter& operator = (const StructureWriter&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* STRUCTUREWRITER_HPP */

