/*
 * File:   MarketingNano.hpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 17:36
 */

#ifndef _CTO_MARKETING_NANO_CLASS_
#define	_CTO_MARKETING_NANO_CLASS_

#include "Partner.hpp"
#include "Rank.hpp"
#include <map>

namespace nnpcto {
namespace cto {

class MarketingNano
{
public:
	MarketingNano();

	virtual ~MarketingNano();

	void compress(std::map<long, Partner*>&);
	void calculateGroupPurchase(std::map<long, Partner*>&);
	void calculateRanks(std::map<long, Partner*>&);
	void calculateBonuses(std::map<long, Partner*>&);

private:
	Rank **_ranks;

private:
	MarketingNano(const MarketingNano&);
	MarketingNano& operator = (const MarketingNano&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_MARKETING_NANO_CLASS_ */

