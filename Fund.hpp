/*
 * File:   Fund.hpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 11:57
 */

#ifndef _CTO_FUND_CLASS_
#define	_CTO_FUND_CLASS_

#include "PartnerProject.hpp"

namespace nnpcto {
namespace cto {

class Fund
{
public:
	Fund(char id, float percent);
	virtual ~Fund();

	char getId() const;

	float getPercent() const;

	void addPartner(PartnerProject* partner);
	unsigned long getPartnersCount() const;

	void setStructurePurchase(unsigned long purchase);

	float getBonus() const;

private:
	char _id;
	float _percent;
	unsigned long _partners;
	float _bonusSize;

private:
	Fund(const Fund&);
	Fund& operator = (const Fund&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* FUND_HPP */

