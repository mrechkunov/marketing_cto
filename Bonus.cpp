/*
 * File:   Bonus.cpp
 * Author: alexander
 *
 * Created on 8 Апрель 2013 г., 10:22
 */

#include "Bonus.hpp"
#include "PartnerProject.hpp"

using namespace nnpcto::cto;

Bonus::Bonus(PartnerProject *source, char type, unsigned char percent, float amount, unsigned char depth)
	: _source(source), _type(type), _percent(percent), _bonus(amount), _depth(depth)
{}

Bonus::Bonus(const Bonus&)
{
	throw "Not supported";
}

Bonus::~Bonus()
{}

Bonus& Bonus::operator = (const Bonus&)
{
	throw "Not supported";
}

PartnerProject* Bonus::getSourcePartner()
{
	return _source;
}

const PartnerProject* Bonus::getSourcePartner() const
{
	return _source;
}

unsigned char Bonus::getPercent() const
{
	return _percent;
}

char Bonus::getType() const
{
	return _type;
}

float Bonus::getAmount() const
{
	return _bonus;
}

unsigned char Bonus::getDepth() const
{
	return _depth;
}
