/*
 * File:   main.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 12:41
 */

#include "StructureLoader.hpp"
#include "MarketingVirta.hpp"
#include "MarketingNano.hpp"
#include "MarketingProsto.hpp"
#include "MarketingCto.hpp"
#include "StructureWriter.hpp"
#include <cstdlib>
#include <iostream>
#include <nnpcto/db/defines.hpp>
#include <nnpcto/db/postgresql/Factory.hpp>

using namespace nnpcto;

/*
 *
 */
int main(int argc, char** argv)
{
	db::IConnection::Options opts;
	db::Connection c;
	std::map<long, cto::Partner*> partners;
	unsigned char periodId = 87;

	try {
		db::postgresql::Factory factory(true);

		opts.setOption("hostname", "10.255.4.51");
		opts.setOption("username", "pgsql");
		opts.setOption("database", "cto");
		opts.setOption("password", "lJzvGjqASp");
		c = db::DriverManager::createConnection(factory.DRIVER_NAME, opts);

		cto::StructureLoader loader(c);
		loader.loadStructure(partners);
		loader.loadPurchases(partners, periodId, 1);
		loader.loadPurchases(partners, periodId, 2);
		loader.loadPurchases(partners, periodId, 3);
		loader.loadPurchases(partners, periodId, 4);

		cto::MarketingVirta virta;

		virta.calculateGroupPurchase(partners);
		virta.compress(partners);
		virta.calculateRanks(partners);
		virta.calculateBonuses(partners);

		cto::MarketingNano nano;

		nano.calculateGroupPurchase(partners);
		nano.compress(partners);
		nano.calculateRanks(partners);
		nano.calculateBonuses(partners);

		cto::MarketingProsto pro;

		pro.calculateGroupPurchase(partners);
		pro.compress(partners);
		pro.calculateRanks(partners);
		pro.calculateFunds(partners);
		pro.calculateBonuses(partners);

		cto::MarketingCto cto;

		cto.calculateGroupPurchase(partners);
		cto.compress(partners);
		cto.calculateRanks(partners);
		cto.calculateFunds(partners);
		cto.calculateUnqualifiedBonuses(partners);
		cto.calculateBonuses(partners);

		cto::StructureWriter writer;

		writer.save(partners, periodId);
		writer.saveQualifications(partners, periodId);
		writer.saveBonuses(partners, periodId);

		loader.destroyStructure(partners);
	}
	catch (const char* excp) {
		std::cout << "Error: " << excp << std::endl;
	}

	return 0;
}

