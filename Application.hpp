/*
 * File:   Application.hpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 13:11
 */

#ifndef _CTO_APPLICATION_CLASS_
#define	_CTO_APPLICATION_CLASS_

namespace nnpcto {
namespace cto {

class Application
{
public:
	enum Projects {
		CTO    = 1,
		VIRTA  = 2,
		NANO   = 3,
		PRO100 = 4
	};

	enum BonusTypes {
		BONUS_LO = 1,
		BONUS_GO = 2,
		BONUS_LEADING = 3,
		BONUS_CLUB = 4
	};

public:
	~Application();
	static const Application* getInstance();
	unsigned char getProjectsCount() const;

private:
	static Application* _instance;
	unsigned char _projectsCount;

private:
	Application();
	Application(const Application& source);
};

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_APPLICATION_CLASS_ */

