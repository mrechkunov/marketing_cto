/*
 * File:   Bonus.hpp
 * Author: alexander
 *
 * Created on 8 Апрель 2013 г., 10:22
 */

#ifndef _NNPCTO_CTO_BONUS_CLASS_
#define	_NNPCTO_CTO_BONUS_CLASS_

namespace nnpcto {
namespace cto {

class PartnerProject;

class Bonus
{
public:
	Bonus(PartnerProject *_source, char type, unsigned char percent, float amount, unsigned char depth);
	~Bonus();

	PartnerProject* getSourcePartner();
	const PartnerProject* getSourcePartner() const;

	unsigned char getPercent() const;
	char getType() const;
	unsigned char getDepth() const;
	float getAmount() const;

private:
	PartnerProject *_source;
	char _type;
	unsigned char _percent;
	unsigned char _depth;
	float _bonus;

private:
	Bonus(const Bonus&);
	Bonus& operator = (const Bonus&);
}; // class Bonus

} // namespace cto
} // namespace nnpcto

#endif	/* _NNPCTO_CTO_BONUS_CLASS_ */

