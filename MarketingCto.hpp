/*
 * File:   MarketingCto.hpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 11:10
 */

#ifndef _CTO_MARKETING_CTO_CLASS_
#define	_CTO_MARKETING_CTO_CLASS_

#include "Partner.hpp"
#include "Rank.hpp"
#include "Fund.hpp"
#include <map>

namespace nnpcto {
namespace cto {

class MarketingCto
{
public:
	MarketingCto();

	virtual ~MarketingCto();

	void compress(std::map<long, Partner*>&);
	void calculateGroupPurchase(std::map<long, Partner*>&);
	void calculateRanks(std::map<long, Partner*>&);
	void calculateFunds(std::map<long, Partner*>& partners);
	void calculateUnqualifiedBonuses(std::map<long, Partner*>& partners);
	void calculateBonuses(std::map<long, Partner*>&);

private:
	Rank **_ranks;
	Fund **_funds;

private:
	MarketingCto(const MarketingCto&);
	MarketingCto& operator = (const MarketingCto&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_MARKETING_CTO_CLASS_ */

