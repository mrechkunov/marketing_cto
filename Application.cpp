/*
 * File:   Application.cpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 13:11
 */

#include "Application.hpp"
#include <cstdlib>

using namespace nnpcto::cto;

Application* Application::_instance = NULL;

Application::Application()
{
	_projectsCount = 4;
}

Application::Application(const Application& source)
{

}

Application::~Application()
{

}

const Application* Application::getInstance()
{
	if (_instance == NULL) {
		_instance = new Application();
		if (_instance == NULL) {
			throw "Memory allocation exception";
		}
	}
	return _instance;
}

unsigned char Application::getProjectsCount() const
{
	return _projectsCount;
}