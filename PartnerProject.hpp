/*
 * File:   PartnerProject.hpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 13:47
 */

#ifndef _CTO_PARTNER_PROJECT_CLASS_
#define	_CTO_PARTNER_PROJECT_CLASS_

#define _CTO_PARTNER_PROJECT_IS_ACTIVE_ 0x01
#define _CTO_PARTNER_PROJECT_IS_QUALIFIED_ 0x02
#define _CTO_PARTNER_PROJECT_IS_PREV_QUALIFIED_ 0x04
#define _CTO_PARTNER_PROJECT_IS_REGISTERED_ 0x08
#define _CTO_PARTNER_PROJECT_IS_PREV_REGISTERED_ 0x10

#include "Bonus.hpp"
#include "Partner.hpp"
#include <list>

namespace nnpcto {
namespace cto {

class Rank;
class Fund;

class PartnerProject
{
public:
	PartnerProject(Partner* partner);
	~PartnerProject();

public:
	long getId() const {
		return _partner->getId();
	};

	long getUserId() const {
		return _partner->getUserId();
	}

	Partner* getPartner();
	const Partner* getPartner() const;

	unsigned long getStructureSize();

	unsigned char getDepth();

	unsigned long getPurchase() const;
	void setPurchase(unsigned long value);

	unsigned long getGroupPurchase() const;
	unsigned long getGroupPurchaseFiveLevels() const;
	unsigned long getSeparation();
	void addGroupPurchase(unsigned long purchase, unsigned char depth);

	bool getIsActive() const;
	void setIsActive();

	bool getIsQualified() const;
	bool getIsPreviousQualified() const;
	void setIsQualified(bool previousQualified = false);

	bool getIsRegistered() const;
	bool getIsPreviousRegistered() const;
	void setIsRegistered(bool previousRegistration = false);

	Rank* getRank() const;
	void setRank(Rank* rank);

	Fund* getFund() const;
	void setFund(Fund* fund);

	void setPurchaseBonus(unsigned char percent);
	void addBonus(PartnerProject* partner, char bonusType, unsigned char percent, unsigned char depth);
	void setStructureBonus(float bonus);
	void addStructureBonus(float bonus);
	void bindStructureBonus();
	void setRegisterAmount(unsigned char amount);

	double getPurchaseBonus() const;
	double getGroupPurchaseBonus() const;
	double getLeadingBonus() const;
	double getStructureBonus() const;
	double getBonus() const;
	unsigned char getRegisterAmount() const;

	const std::list<Bonus*>& getBonuses() const;

public:
	PartnerProject* getParent();
	const PartnerProject* getParent() const;

	void addChild(PartnerProject *child);
	void compress();

// --------------------------------------------------------------------------------------------
// Маркетинг план
private:
	//
	Partner *_partner;

	// размер структуры партнёра (количество активных партнёров)
	unsigned long _size;

	// глубина партнёра относительно корня структуры в текущем проекте
	unsigned char _depth;

	// закупка партнёра в текущем проекте
	unsigned long _lo;

	// групповая закупка
	unsigned long _go;

	// ГО в пяти уровнях
	unsigned long _go5;

	// Бонус за ЛО
	float _loBonus;

	// Бонус за ГО
	double _goBonus;

	// лидерский бонус
	double _leadingBonus;

	// бонус с оборота компании
	double _structureBonus;

	// ранг партнёра
	Rank* _rank;

	// фонд или клуб партнёра
	Fund* _fund;

	// бонусы партнёра
	std::list<Bonus*> _bonuses;

	// флаги партнёра
	char _flags;

	// регистрационный сбор
	unsigned char _register;

// --------------------------------------------------------------------------------------------
// Дерево/структура
private:
	PartnerProject *_parent;
	PartnerProject *_firstChild;
	PartnerProject *_lastChild;
	PartnerProject *_nextSibling;
	unsigned short _childrenCount;

private:
	PartnerProject(const PartnerProject& source);
	PartnerProject& operator = (const PartnerProject& source);
}; // class PartnerProject

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_PARTNER_PROJECT_CLASS_ */

