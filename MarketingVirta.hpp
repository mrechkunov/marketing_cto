/*
 * File:   MarketingVirta.hpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 14:29
 */

#ifndef _CTO_MARKETING_VIRTA_CLASS_
#define	_CTO_MARKETING_VIRTA_CLASS_

#include "Partner.hpp"
#include "Rank.hpp"
#include <map>

namespace nnpcto {
namespace cto {

class MarketingVirta
{
public:
	MarketingVirta();
	virtual ~MarketingVirta();

	void compress(std::map<long, Partner*>&);
	void calculateGroupPurchase(std::map<long, Partner*>&);
	void calculateRanks(std::map<long, Partner*>&);
	void calculateBonuses(std::map<long, Partner*>&);

private:
	Rank **_ranks;

private:
	MarketingVirta(const MarketingVirta&);
	MarketingVirta& operator = (const MarketingVirta&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_MARKETING_VIRTA_CLASS_ */

