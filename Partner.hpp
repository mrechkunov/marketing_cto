/*
 * File:   Partner.hpp
 * Author: alexander
 *
 * Created on 3 Апрель 2013 г., 12:41
 */

#ifndef _CTO_PARTNER_CLASS_
#define	_CTO_PARTNER_CLASS_

#include <cstdlib>

namespace nnpcto {
namespace cto {

class PartnerProject;

class Partner
{
public:
	Partner(long id, long userId);
	~Partner();

	void addChild(Partner *child);

	PartnerProject* getPartner(unsigned char projectId);
	const PartnerProject* getPartner(unsigned char projectId) const;

	long getId() const {
		return _id;
	}

	long getUserId() const {
		return _userId;
	}

	unsigned long getGroupPurchase() const;
	unsigned long getGroupPurchaseFiveLevels() const;

	unsigned long getBonus() const;

// --------------------------------------------------------------------------------------------
// Маркетинг план
private:
	long _id;
	long _userId;
	PartnerProject** _projectPartners;

// --------------------------------------------------------------------------------------------
// Дерево/структура
private:
	Partner *_parent;
	Partner *_firstChild;
	Partner *_lastChild;
	Partner *_nextSibling;
	unsigned short _childrenCount;

private:
	static unsigned char _projectsCount;
	static unsigned char _getProjectsCount();

private:
	Partner(const Partner&);
	Partner& operator = (const Partner&);

}; // class Partner

} // namespace cto
} // namespace nnpcto

#endif	/* PARTNER_HPP */
