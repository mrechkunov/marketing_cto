/*
 * File:   MarketingProsto.hpp
 * Author: alexander
 *
 * Created on 4 Апрель 2013 г., 10:28
 */

#ifndef _CTO_MARKETING_PRO_STO_CLASS_
#define	_CTO_MARKETING_PRO_STO_CLASS_

#include "Partner.hpp"
#include "Rank.hpp"
#include "Fund.hpp"
#include <map>

namespace nnpcto {
namespace cto {

class MarketingProsto
{
public:
	MarketingProsto();
	virtual ~MarketingProsto();

	void compress(std::map<long, Partner*>&);
	void calculateGroupPurchase(std::map<long, Partner*>&);
	void calculateRanks(std::map<long, Partner*>&);
	void calculateFunds(std::map<long, Partner*>& partners);
	void calculateBonuses(std::map<long, Partner*>&);

private:
	Rank **_ranks;
	Fund **_funds;

private:
	MarketingProsto(const MarketingProsto&);
	MarketingProsto& operator = (const MarketingProsto&);
};

} // namespace cto
} // namespace nnpcto

#endif	/* _CTO_MARKETING_PRO_STO_CLASS_ */

